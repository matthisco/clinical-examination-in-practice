import React, {useState, useEffect} from 'react';

import {
  View,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  FlatList,
  Dimensions,
  StyleSheet,
  StatusBar,
  Platform,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {useSelector, useDispatch} from 'react-redux';
import {ListItem, SearchBar} from 'react-native-elements';
import {filteredVideo, initSearch} from '../actions/videos';
import StyledText from './StyledText';
const Search = props => {
  const {
    navigation: {navigate},
    route: {name},
  } = props;

  const search = useSelector(state => state.ceipApp.search);
  const dispatch = useDispatch();
  const iconStyle = {type: 'fontAwesome', color: '#86939e', name: 'fa-search'};

  const SearchFilterFunction = term => {
    const action = filteredVideo(term);
    dispatch(action);
  };

  const onSubmitEdit = id => {
    dispatch(initSearch());
    navigate('Video Player', {id});
  };

  const renderItem = ({item}) => {
    return (
      <Text onPress={() => onPress(item)} style={style.itemStyle}>
        {item.title}
      </Text>
    );
  };
  return (
    <View>
      {name == 'Home' || name == 'Video Episodes' || name == 'Tests' ? (
        <SearchBar
          round
          containerStyle={styles.containerStyle}
          inputStyle={{backgroundColor: 'white'}}
          inputContainerStyle={{
            borderRadius: 5,
            backgroundColor: 'white',
          }}
          icon={iconStyle}
          onSubmitEditing={onSubmitEdit}
          searchIcon={{size: 18}}
          onChangeText={text => SearchFilterFunction(text)}
          onClear={text => SearchFilterFunction('')}
          placeholder="Type Here..."
          value={search.term}
        />
      ) : null}
      {name == 'Home' ? (
        <View>
          <FlatList
            data={search.videos}
            renderItem={renderItem}
            contentContainerStyle={{overflow: 'hidden'}}
            keyExtractor={item => item.id.toString()}
            ItemSeparatorComponent={separator}
          />
        </View>
      ) : null}
    </View>
  );
};

export default Search;
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: 10,
    borderColor: '#ffffff50',
    borderBottomWidth: 1,
    marginTop: Platform.OS === 'ios' ? 20 : 0,
    paddingTop: Platform.OS === 'ios' ? 20 : 10,
  },
  separator: {
    borderBottomColor: '#d1d0d4',
    borderBottomWidth: 1,
  },
  containerStyle: {
    borderTopWidth: 0,
    borderBottomWidth: 0,
    backgroundColor: 'transparent',
  },
  itemStyle: {backgroundColor: '#00336680', color: 'white', padding: 10},
  title: {
    color: '#fff',
    width: Dimensions.get('window').width,
    fontSize: 18,
  },
});
