import {CommonActions} from '@react-navigation/native';

function goBack(sceneKey) {
	NavigationActions.back({ key: sceneKey });
}

let _navigator;

function setTopLevelNavigator(navigatorRef) {
	_navigator = navigatorRef;
}

function navigate(routeName, params) {
	_navigator.dispatch(
		NavigationActions.navigate({
			routeName,
			params
		})
	);

	const pushAction = CommonActions.push({
		routeName,
		params
	});

	_navigator.dispatch(pushAction);
}

const pushIt = (routeName, params) => CommonActions.push({ routeName, params });

function getParams() {
	const {
		nav: { index: navIndex = "", routes: navRoutes = "" } = {},
		routes = "",
		index = ""
	} = _navigator.state;

	const params =
		navRoutes[navIndex].params ||
		navRoutes[navIndex].routes[navRoutes[navIndex].index].params;

	return params;
}

export default {
	navigate,
	pushIt,
	goBack,
	setTopLevelNavigator,
	getParams
};
