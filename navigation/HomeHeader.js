import React, {useState, useEffect} from 'react';

import {connect} from 'react-redux';
import {
  Image,
  View,
  Text,
  Modal,
  Button,
  TouchableOpacity,
  AsyncStorage,
  StyleSheet,
  Platform,
  Alert,
  FlatList,
  StatusBar,
  TouchableHighlight,
} from 'react-native';
import {CommonActions} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {ListItem, SearchBar} from 'react-native-elements';
import {useSelector, useDispatch} from 'react-redux';
import {filteredVideo, initSearch} from '../actions/videos';
export default function HomeHeader(props) {

  const dispatch = useDispatch();

  const {
    navigation: {navigate},
  } = props;

  const SearchFilterFunction = term => {
    const action = filteredVideo(term);
    dispatch(action);
  };

  const loadRecapVideo = id => {
    navigate('Video Player', {id});
  };

  const onSubmitEdit = id => {
    navigate('Video Player', {id});
  };

  const onPress = item => {
    const {
      navigation: {navigate},
    } = props;
    dispatch(initSearch());

    navigate('Video Player', {id: item.id});
  };
  const separator = () => <View style={styles.separator} />;

  const renderItem = ({item}) => {
    return (
      <Text
        onPress={() => onPress(item)}
        style={{
          backgroundColor: '#00336680',
          color: 'white',
          padding: 10,
        }}>
        {item.title}
      </Text>
    );
  };

  return (
    <View>
      <View style={styles.container}>
        <Image
          source={require('../assets/images/CEIP_logo.png')}
          style={{width: 120, height: 50}}
        />

        <Image
          source={require('../assets/images/connect-logo.png')}
          style={{width: 50, height: 60}}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
    marginTop: Platform.OS === 'ios' ? 20 : 0,
    paddingTop: Platform.OS === 'ios' ? 20 : 10,
  },
  title: {
    color: '#fff',
    fontSize: 18,
  },
  separator: {
    borderBottomColor: '#d1d0d4',
    borderBottomWidth: 1,
  },
});
