import React, {Component, useRef} from 'react';
import {
  View,
  Text,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  TouchableOpacity,
  Image,
} from 'react-native';
import Share from 'react-native-share';
import {Button} from 'react-native-elements';
import ViewShot from 'react-native-view-shot';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import ImageButton from '../components/ImageButton';
import StyledText from '../components/StyledText';
import TextWithFont from '../components/TextWithFont';
class ShareResult extends React.Component {
  constructor(props) {
    super(props);
  }

  onCapture = uri => {
    const {title} = this.props;

    this.refs.viewShot.capture().then(uri => {
      const shareOptions = {
        title: 'Share via',
        subject: `Check out my score for the ${title} module on the Tomorrow’s Clinicians app. Head over to www.tomorrowsclinicians.co.uk to try the app yourself.`,
        url: uri,
        message: `Check out my score for the ${title} module on the Tomorrow’s Clinicians app. Head over to www.tomorrowsclinicians.co.uk to try the app yourself.`,
      };
      Share.open(shareOptions).catch(err => console.log(err));
    });
  };

  render() {
    const {
      questions,
      completed,
      current,
      results: {totalScore},
      title,
      icon,
      id,
      total,
      resetQuiz,
    } = this.props;

    return (
      <View style={{flex: 1}}>
        <ViewShot
          ref="viewShot"
          options={{format: 'jpg', quality: 0.9}}
          style={styles.viewShot}>
          <StyledText text="TEST YOURSELF" style={styles.textResult} bold />
          <StyledText text={title} style={styles.textResult} bold />
          <AnimatedCircularProgress
            size={200}
            style={styles.chart}
            width={10}
            fill={totalScore}
            tintColor="#fff"
            backgroundColor="#3d587580">
            {fill => (
              <>
                <TextWithFont style={styles.scoreText}>SCORE:</TextWithFont>
                <TextWithFont style={styles.score}>{totalScore} %</TextWithFont>
              </>
            )}
          </AnimatedCircularProgress>
          <View style={styles.layout}>
            <View
              style={{
                alignItems: 'flex-start',
              }}>
              <Image
                source={require('../assets/images/ia-logo.png')}
                resizeMode="contain"
                style={styles.iaLogo}
              />
              <StyledText
                text="tomorrowsclinicians.co.uk"
                style={styles.textUrl}
                bold
              />
            </View>
            <View style={{alignSelf: 'flex-end'}}>
              <Image
                source={require('../assets/images/iClinicalLogo.png')}
                resizeMode="contain"
                style={styles.iClinicalLogo}
              />
            </View>
          </View>
        </ViewShot>
        <View style={{backgroundColor: '#006666', padding: 10}}>
          <ImageButton
            text="SHARE YOUR SCORE"
            image="share-icon"
            onClick={this.onCapture}
          />
          <Button
            title="RESTART QUIZ"
            buttonStyle={styles.button}
            onPress={() => resetQuiz(id)}
          />
        </View>
      </View>
    );
  }
}

export default ShareResult;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexWrap: 'wrap',
    textAlign: 'left',
    flexDirection: 'row',
    alignSelf: 'stretch',
    color: '#fff',
    lineHeight: 2,
  },
  layout: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    width: 400,
    height: 100,
    padding: 5,
    zIndex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  viewShot: {
    backgroundColor: '#003366',
    width: 400,
    paddingBottom: 50,
    paddingTop: 10,
  },
  textResult: {
    color: 'white',
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
    marginBottom: 10,
    padding: 10,
  },
  iaLogo: {width: 90, height: 50, margin: 5, marginLeft: 15},
  iClinicalLogo: {
    alignSelf: 'flex-start',
    width: 100,
    height: 50,
    margin: 5,
    marginRight: 10,
  },
  chart: {
    alignSelf: 'center',
  },
  textUrl: {textTransform: 'lowercase', marginLeft: 20},
  score: {
    color: 'white',
    fontSize: 50,
  },
  scoreText: {
    color: 'white',
    fontSize: 20,
  },
  button: {
    margin: 20,
    marginBottom: 40,
    flex: 6,
    padding: 10,
    backgroundColor: '#0CA8A8',
  },
});
