    const resetAction = CommonActions.reset({
                index: 0,
                actions: [
                  NavigationActions.navigate({
                    routeName: 'Drawer',
                  }),
                ],
              });
    
              const navigateAction = NavigationActions.navigate({
                routeName: 'Tests',
              });
    
              navigation.dispatch(resetAction);
              navigation.dispatch(navigateAction);