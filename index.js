/**
 * @format
 */
import React, { Component } from "react";
import {AppRegistry} from 'react-native';
import App from './App';
import app from './app.json';

import {Provider} from 'react-redux';

import {store, persistor} from './config/configureStore';
const Root = () => (
	<Provider store={store}>
		<App />
	</Provider>
);

AppRegistry.registerComponent(app.name, () => Root);
