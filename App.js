/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import * as React from 'react';
import {
  Button,
  View,
  Text,
  ImageBackground,
  StyleSheet,
  Dimensions,
} from 'react-native';

import NavigationService from './navigation/NavigationService';
import AppNavigator from './navigation/AppNavigator';

import {store, persistor} from './config/configureStore';

import {PersistGate} from 'redux-persist/integration/react';

export default function App() {
  return (
    <ImageBackground
      source={require('./assets/images/CEIP_cover.jpg')}
      style={styles.container}>
      <PersistGate loading={null} persistor={persistor}>
        <AppNavigator />
      </PersistGate>
    </ImageBackground>
  );
}

var styles = StyleSheet.create({
  container: {
    position: 'absolute',
    left: 0,
    top: 0,
    backgroundColor: 'transparent',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
});
