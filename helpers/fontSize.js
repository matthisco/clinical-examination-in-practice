import { Dimensions } from "react-native";

const { width } = Dimensions.get("window");
const textFontSize = width * 0.03;

export default textFontSize;
