import { combineReducers } from "redux";
import { ceipApp } from "./videos";

const rootReducer = combineReducers({
	ceipApp
});

export default rootReducer;
