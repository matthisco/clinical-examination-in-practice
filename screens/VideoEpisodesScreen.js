import React, {useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  Image,
  View,
  ActivityIndicator,
} from 'react-native';
import {BackHandler} from 'react-native';
import {useSelector} from 'react-redux';
import {CommonActions} from '@react-navigation/native';
import VideoList from '../components/VideoList';
import DrawerHeader from '../navigation/DrawerHeader';
import NavigationService from '../navigation/NavigationService.js';

export default function VideoEpisodesScreen(props) {
  const videos = useSelector(state => state.ceipApp.videos);

  const searchedVideos = useSelector(state => state.ceipApp.search.videos);

  React.useEffect(() => {
    BackHandler.removeEventListener('hardwareBackPress', onBackPress);

    // returned function will be called on component unmount
    return () => {
      BackHandler.addEventListener('hardwareBackPress', onBackPress);
    };
  }, []);

  const onBackPress = () => {
    const {navigation} = props;

    navigation.goBack();
    return true;
  };

  let videoList =
    searchedVideos.length > 0
      ? searchedVideos
      : videos.filter(video => video.title.indexOf('QR') == -1);

  if (videos == '') {
    return (
      <>
        <DrawerHeader {...props} />
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#fff" />
        </View>
      </>
    );
  }

  return (
    <>
      <DrawerHeader {...props} />
      <ScrollView contentContainerStyle={styles.scrollContainer}>
        <VideoList videos={videoList} {...props.navigation} />
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  scrollContainer: {
    flexGrow: 1,
    paddingBottom: 50,
    marginBottom: 50,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingTop: 15,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
