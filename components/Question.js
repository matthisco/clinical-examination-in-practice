import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Button,
  Dimensions,
  TouchableHighlight
} from "react-native";
import TextWithFont from "./TextWithFont";
export default class Question extends React.Component {
  constructor() {
    super();
  }

  _onPressButton = (index, answer, id) => {
    const { onSelect } = this.props;
    onSelect(index, answer, id);
  };

  renderOptions = question => {
    const { id, current, results } = this.props;
    const result = [];

    let answers = question.answers.split("|");

    answers.forEach((item, index) => {
      let answerColor = question.correctAnswer == index ? "green" : "#B20000";
      let answeredColor =
        results.correctAnswers.includes(current) ||
        results.incorrectAnswers.includes(current)
          ? answerColor
          : "#135291";
      result.push(
        <TouchableHighlight
          key={index}
          style={[styles.button, { backgroundColor: answeredColor }]}
          underlayColor="#505050"
          activeOpacity={1}
          onPress={() => {
            !results.correctAnswers.includes(current) &&
            !results.incorrectAnswers.includes(current)
              ? this._onPressButton(item, index, id)
              : null;
          }}
        >
          <TextWithFont style={styles.buttonText}>{item.trim()}</TextWithFont>
        </TouchableHighlight>
      );
    });

    return result;
  };

  render() {
    const { question, answers, title, total, current } = this.props;

    return (
      <View>
        <TextWithFont style={styles.questionText}>
          Q{current + 1} {this.props.question.question}
        </TextWithFont>

        {this.renderOptions(question)}
      </View>
    );
  }
}

const getWidth = () => {
  const screenWidth = Dimensions.get("window").width;

  return screenWidth;
};

const styles = StyleSheet.create({
  button: {
    width: getWidth() - 20,
    padding: 10,
    marginLeft: 20,
    marginBottom: 10,
    alignSelf: "center"
  },
  buttonText: {
    color: "white",
    fontSize: 16
  },
  questionText: {
    fontSize: 20,
    fontWeight: "bold",
    color: "#fff",
    backgroundColor: "#135291",
    padding: 10,
    marginBottom: 10
  },
  questionCount: {
    fontSize: 16,
    color: "#fff",
    width: "50%",
    textAlign: "right"
  },
  title: {
    fontSize: 16,
    color: "#fff",
    width: "50%",
    textAlign: "left"
  }
});
