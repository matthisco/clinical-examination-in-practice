import React, {Component} from 'react';
import {
  BackHandler,
  StyleSheet,
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  AppState,
  Dimensions,
  Platform,
  StatusBar,
} from 'react-native';
import StyledText from './StyledText';
import VideoPlayerHeader from './VideoPlayerHeader';
import ShareComponent from './Share';
import {WebView} from 'react-native-webview';

export default class VideoPlayer extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      appState: AppState.currentState,
    };
  }

  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = nextAppState => {
    this.setState({appState: nextAppState});
  };

  render() {
    let {
      id,
      icon,
      title,
      bookMarked,
      description,
      youtubeVideo,
      preview,
      url,
    } = this.props;

    return (
      <View style={styles.container}>
        {this.state.appState == 'active' && (
          <WebView
            mediaPlaybackRequiresUserAction={true}
            allowsFullscreenVideo={true}
            style={{
              height: undefined,
              width: '100%',
              alignSelf: 'center',
              alignContent: 'center',
              aspectRatio: 16 / 9,
            }}
            source={{
              uri: `https://www.youtube.com/embed/${youtubeVideo}?rel=0`,
            }}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'black',
    borderWidth: 2,
    borderColor: '#ffffff90',
    margin: 10
  },
});
