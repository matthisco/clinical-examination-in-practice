import React, {Component} from 'react';
import {
	Image,
	View,
	Text,
	Modal,
	Button,
	TouchableOpacity,
	AsyncStorage,
	StyleSheet,
	Platform,
	Alert,
	Dimensions,
	TouchableHighlight,
} from 'react-native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer, DefaultTheme} from '@react-navigation/native';

import HomeScreen from '../screens/HomeScreen';
import AboutScreen from '../screens/AboutScreen';
import MyResultsScreen from '../screens/MyResultsScreen';
import TestsScreen from '../screens/TestsScreen';
import BookmarkedVideosScreen from '../screens/BookmarkedVideosScreen';
import TestYourselfScreen from '../screens/TestYourselfScreen';
import VideoEpisodesScreen from '../screens/VideoEpisodesScreen';
import VideoPlayerScreen from '../screens/VideoPlayerScreen';

import VideoHeader from './VideoHeader';
import DrawerHeader from './DrawerHeader';
import HomeHeader from './HomeHeader';
import SideMenu from './SideMenu';

const appTheme = {
	...DefaultTheme,
	colors: {
		...DefaultTheme.colors,
		// primary: '#00003390',
		card: '#00003399',
		background: '#00003350',
	},
};

const drawerOptions = {
	transparentCard: true,
	backgroundColor: 'transparent',
	cardStyle: {
		backgroundColor: 'transparent',
	},

	transitionConfig: () => ({
		containerStyle: {
			backgroundColor: 'transparent',
			opacity: 1,
		},
	}),
};

const Drawer = createDrawerNavigator();

export default function AppNavigator() {
	return (
		<NavigationContainer theme={appTheme}>
			<Drawer.Navigator
				initialRouteName="Home"
				drawerContent={props => <SideMenu {...props} />}>
				<Drawer.Screen
					name="Home"
					component={HomeScreen}
					options={drawerOptions}
				/>
				<Drawer.Screen
					name="Video Episodes"
					component={VideoEpisodesScreen}
					options={drawerOptions}
				/>
				<Drawer.Screen
					name="Test Yourself"
					component={TestYourselfScreen}
					options={drawerOptions}
				/>
				<Drawer.Screen
					name="Tests"
					component={TestsScreen}
					options={drawerOptions}
				/>
				<Drawer.Screen
					name="My Results"
					component={MyResultsScreen}
					options={drawerOptions}
				/>
				<Drawer.Screen
					name="About"
					component={AboutScreen}
					options={drawerOptions}
				/>
				<Drawer.Screen
					name="Bookmarked Videos"
					component={BookmarkedVideosScreen}
					options={drawerOptions}
				/>
				<Drawer.Screen name="Video Player" component={VideoPlayerScreen} />
			</Drawer.Navigator>
		</NavigationContainer>
	);
}
