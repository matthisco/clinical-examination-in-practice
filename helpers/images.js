const images = {
	chest: {
		imgName: "chest",
		uri: require("../assets/images/chest.png")
	},
	head: {
		imgName: "head",
		uri: require("../assets/images/head.png")
	},
	arms: {
		imgName: "arms",
		uri: require("../assets/images/arms.png")
	},
	legs: {
		imgName: "legs",
		uri: require("../assets/images/legs.png")
	},
	mid: {
		imgName: "mid",
		uri: require("../assets/images/mid.png")
	},
	"share-icon": {
		imgName: "share-icon",
		uri: require("../assets/images/share-icon.png")
	},
	"test-yourself": {
		imgName: "test-yourself",
		uri: require("../assets/images/test-yourself.png")
	},
};

export { images };
