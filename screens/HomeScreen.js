import React, {useState, useEffect} from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  ImageBackground,
  ActivityIndicator,
  TouchableOpacity,
  View,
} from 'react-native';

import {useSelector, useDispatch} from 'react-redux';
import {fetchData, loadState} from '../actions/videos';
import ResponsiveImage from 'react-native-responsive-image';
import HomeMenu from '../components/HomeMenu';
import HomeHeader from '../navigation/HomeHeader';
import DrawerHeader from '../navigation/DrawerHeader';

export default function HomeScreen(props) {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchData());
  }, []);

  return (
    <View style={styles.container}>
      <HomeHeader {...props} />

      <HomeMenu {...props} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
