import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Platform,
  Dimensions,
  TouchableHighlight,
} from 'react-native';
import {CommonActions} from '@react-navigation/native';
import CustomImage from './CustomImage';

export default class VideoEpisode extends React.Component {
  constructor(props) {
    super(props);
    this.state = {height: 215};
  }

  render() {
    const {
      id,
      title,
      description,
      video,
      preview,
      push,
      navigate,
      testLink,
    } = this.props;

    return (
      <View style={{flexDirection: 'row'}}>
        <TouchableHighlight
          style={styles.episode}
          activeOpacity={1}
          underlayColor="#808080"
          onPress={() => {
            navigate(testLink ? 'Test Yourself' : 'Video Player', {id});
          }}>
          <Text style={styles.text}>{title}</Text>
        </TouchableHighlight>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  episode: {
    flex: 1,
    margin: 1,
    opacity: 0.7,
    marginTop: 2,
    padding: 5,
    paddingLeft: 10,
    justifyContent: 'flex-start',
    backgroundColor: '#003366',
  },
  title: {
    color: 'white',
  },
  text: {
    color: 'white',
    fontSize: 16,
    justifyContent: 'flex-start',
    marginTop: 3,
    marginBottom: 3,
    fontFamily: 'Lato-Regular',
  },
});
