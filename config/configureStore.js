import {createStore, applyMiddleware} from 'redux';
import {persistStore, persistReducer, autoRehydrate} from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import thunk from 'redux-thunk';
import app from "../app.json";
import logger from 'redux-logger';
import rootReducer from '../reducers';

const persistConfig = {
	key: 'root',
	storage: AsyncStorage, // see "Merge Process" section for details.
	whitelist: [app.name],
	timeout: null,
};

const pReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(pReducer, applyMiddleware(thunk, logger));
export const persistor = persistStore(store);

export default store;
