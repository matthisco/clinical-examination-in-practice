import React, {Component} from 'react';

import {connect} from 'react-redux';
import {
  Image,
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Platform,
  Dimensions,
  ImageBackground,
  TouchableHighlight,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome5';
import {ListItem, SearchBar} from 'react-native-elements';
import {filteredVideo, initSearch} from '../actions/videos';
import Homemenu from '../components/HomeMenu';
import StyledText from '../components/StyledText';
class SideMenu extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {navigation} = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <TouchableOpacity
            style={{
              marginLeft: 10,
              marginRight: 5,
            }}
            onPress={() => navigation.closeDrawer()}>
            <View
              style={{
                flexDirection: 'row',
                width: 100,
              }}>
              <Icon
                name="chevron-left"
                size={16}
                color="#fff"
                style={{marginRight: 10, marginTop: 3}}
              />
              <StyledText text="Close" style={{fontSize: 16}} />
            </View>
          </TouchableOpacity>
        </View>
        <Homemenu navigation={navigation} style={styles.homeMenu} />
      </View>
    );
  }
}
const mapDispatchToProps = dispatch => ({
  filteredVideo: data => dispatch(filteredVideo(data)),
  initSearch: () => dispatch(initSearch()),
});

const mapStateToProps = state => {
  return {
    videos: state.ceipApp.videos,
    search: state.ceipApp.search,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SideMenu);

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 50,
    marginTop: Platform.OS === 'ios' ? 20 : 0,
    paddingTop: Platform.OS === 'ios' ? 20 : 10,
  },
  container: {
    height: Dimensions.get('window').height,
  },
  homeMenu: {
    fontSize: 16,
    backgroundColor: 'blue'
  },
});
