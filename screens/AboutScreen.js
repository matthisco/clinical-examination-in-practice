import React, {Component} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
} from 'react-native';
import {WebView} from 'react-native-webview';
import DrawerHeader from '../navigation/DrawerHeader';
import ArrowButton from '../components/ArrowButton';
export default class AboutScreen extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      content: '',
    };
  }
  componentDidMount() {
    fetch('http://app.rguc.co.uk/?rest_route=/wp/v2/pages/804')
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          content: `<!DOCTYPE html><html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"><body style="color:white;font-family: 'arial'">
  ${responseJson.content.rendered}</body></html>`,
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  render() {
    const {content} = this.state;

    if (content == '') {
      return (
        <View style={[styles.container, styles.horizontal]}>
          <DrawerHeader {...this.props} />
          <ActivityIndicator size="large" color="#fff" />
        </View>
      );
    }

    return (
      <>
        <DrawerHeader {...this.props} />
        <View style={styles.container}>
          <WebView
            style={styles.webView}
            originWhitelist={['*']}
            source={{html: content}}
            scalesPageToFit={false}
          />

          <View
            style={{
              paddingBottom: 30,
              paddingTop: 20,
            }}>
            <ArrowButton
              text="Terms and Conditions"
              link="http://www.tomorrowsclinicians.co.uk/terms-and-conditions/"
            />

            <ArrowButton
              text="Privacy Policy"
              link="http://www.tomorrowsclinicians.co.uk/tomorrows-clinicians-privacy-policy/"
            />
          </View>
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    color: 'white',
    paddingTop: 15,
  },
  webView: {
    backgroundColor: '#00669950',
    color: '#fff',
  },
});
