import React, {Component} from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Clipboard,
  ToastAndroid,
  AlertIOS,
  Image,
  Platform,
} from 'react-native';
import Share, {ShareSheet, Button} from 'react-native-share';
import StyledText from './StyledText';
import {images} from '../helpers/images';

export default function ImageButton(props) {
  let {text, bookMarked, image, id, style, onClick} = props;

  let imageSource = images[image].uri;
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => {
        onClick();
      }}>
      <View style={styles.imageContainer}>
        <Image
          style={{
            width: 35,
            height: 35,
            resizeMode: 'contain',
          }}
          source={imageSource}
        />
      </View>
      <View style={styles.textContainer}>
        <StyledText text={text} />
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: 200,

    marginLeft: 25,
    flexDirection: 'row',
  },
  imageContainer: {
    height: 30,
    width: 35,
    marginRight: 10,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  textContainer: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
});
