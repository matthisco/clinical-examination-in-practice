import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  FlatList,
  Dimensions,
  StyleSheet,
  StatusBar,
  Platform,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome5';
import {useSelector, useDispatch} from 'react-redux';
import {CommonActions} from '@react-navigation/native';

import Search from '../components/Search';
import {filteredVideo, initSearch} from '../actions/videos';
import StyledText from '../components/StyledText';
const DrawerHeader = props => {
  const {
    navigation,
    route: {name},
  } = props;
  const dispatch = useDispatch();
  return (
    <>
      <View style={styles.container}>
        <View
          style={{
            alignSelf: 'flex-start',
          }}>
          <TouchableOpacity
            style={{
              width: 32,
              marginLeft: 10,
              marginRight: 5,
            }}
            onPress={() => {
              dispatch(initSearch());
              navigation.goBack();
            }}>
            <Icon name="chevron-left" size={20} color="#fff" />
          </TouchableOpacity>
          <View
            style={{
              marginTop: 40,
              marginLeft: 10,
            }}>
            <StyledText text={name} style={styles.title} />
          </View>
        </View>
        <View>
          <Image
            source={require('../assets/images/CEIP_logo.png')}
            style={{
              width: 120,
              height: 60,
            }}
          />

          <TouchableOpacity
            onPress={() => {
              navigation.openDrawer();
            }}
            style={{
              margin: 10,
              alignSelf: 'flex-end',
            }}>
            <Icon name="bars" size={20} color="#fff" />
          </TouchableOpacity>
        </View>
      </View>
      <Search {...props} />
    </>
  );
};

export default DrawerHeader;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: 10,
    borderColor: '#ffffff50',
    borderBottomWidth: 1,
    marginTop: Platform.OS === 'ios' ? 20 : 0,
    paddingTop: Platform.OS === 'ios' ? 20 : 10,
  },
  containerStyle: {
    borderTopWidth: 0,
    borderBottomWidth: 0,
    backgroundColor: 'transparent',
  },
  itemStyle: {backgroundColor: '#00336680', color: 'white', padding: 10},
  title: {
    color: '#fff',
    width: Dimensions.get('window').width,
    fontSize: 18,
  },
});
