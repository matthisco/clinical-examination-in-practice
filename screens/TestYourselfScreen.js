import React from 'react';
import {
  View,
  Text,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  Picker,
  Dimensions,
  TouchableOpacity,
  Image,
} from 'react-native';
import Share from 'react-native-share';

import {Button} from 'react-native-elements';

import {useSelector, useDispatch} from 'react-redux';

import {AnimatedCircularProgress} from 'react-native-circular-progress';
import FadeInView from 'react-native-fade-in-view';
import StyledText from '../components/StyledText';
import TextWithFont from '../components/TextWithFont';

import {images} from '../helpers/images';
import Question from '../components/Question';
import ShareResult from '../components/ShareResult';
import DrawerHeader from '../navigation/DrawerHeader';
import {
  submitAnswer,
  resetQuiz,
  nextQuestion,
  completeQuiz,
} from '../actions/videos';

export default function TestYourselfScreen(props) {
  const {
    route: {
      params: {id},
    },
    navigation: {navigate},
  } = props;

  const viewShotRef = React.createRef();
  const dispatch = useDispatch();
  const videos = useSelector(state => state.ceipApp.videos);

  const screenWidth = Dimensions.get('window').width;

  let video = videos.find(obj => obj.id == id);
  const {questions, completed, current, results, title, icon, total} = video;

  const questionsFiltered = questions.filter(obj => obj.question != '');

  let chartData = [];
  let imageSource = images[icon].uri;
  let lastQuestion =
    questionsFiltered.length ==
    results.incorrectAnswers.length + results.correctAnswers.length
      ? true
      : false;

  const background = completed === true ? 'transparent' : '#ffffff40';

  chartData.push(results.totalScore);
  const chartConfig = {
    backgroundGradientFrom: '#1E2923',
    backgroundGradientTo: '#08130D',
    strokeWidth: 2,
  };

  const reset = id => {
    dispatch(resetQuiz(id));
  };

  const next = (id, current) => {
    dispatch(nextQuestion({id, current}));
  };

  const checkAnswer = (index, answer, id) => {
    let results = {};

    let video = videos.find(obj => obj.id == id);
    const questionsFiltered = video.questions.filter(obj => obj.question != '');
    let question = questionsFiltered[index];
    let questionsLength = questionsFiltered.length;
    let answers = question.answers.split('|');
    let completed = index === questionsLength - 1 ? true : false;
    let isCorrect = answers[question.correctAnswer].trim() == answer.trim();

    let correctAnswers = video.results.correctAnswers;
    if (isCorrect) {
      correctAnswers.push(index);
    }
    let incorrectAnswers = video.results.incorrectAnswers;
    if (!isCorrect) {
      incorrectAnswers.push(index);
    }

    let currentResult = {
      correctAnswers,
      incorrectAnswers,
    };

    setTimeout(() => {
      dispatch(
        submitAnswer({
          id,
          current: index,
          results: currentResult,
        }),
      );
    }, 1000);
  };

  const completeQuizEvent = id => {
    let video = videos.find(obj => obj.id == id);

    let correctAnswers = video.results.correctAnswers;
    let incorrectAnswers = video.results.incorrectAnswers;
    const questionsFiltered = video.questions.filter(obj => obj.question != '');

    let currentResults = {
      correctAnswers,
      incorrectAnswers,
      totalScore: Math.round(
        (correctAnswers.length / questionsFiltered.length) * 100,
      ),
    };
    dispatch(
      completeQuiz({
        id,
        currentResults,
      }),
    );
  };

  return (
    <>
      <DrawerHeader {...props} />
      <View style={{flex: 1}}>
        <View style={styles.header}>
          <Image
            style={styles.image}
            resizeMode="contain"
            source={imageSource}
          />

          <StyledText text={title} style={styles.title} />
        </View>

        {!completed === true && (
          <View style={{alignItems: 'flex-end', marginTop: 10}}>
            <TextWithFont style={styles.questionCount}>
              Question {current + 1} out of {questionsFiltered.length}
            </TextWithFont>
          </View>
        )}

        <ScrollView
          contentContainerStyle={[
            styles.container,
            {backgroundColor: background},
          ]}>
          <View
            style={{
              backgroundColor: background,
              height: '100%',
              paddingBottom: 40,
            }}>
            {questionsFiltered.length > 0 && !completed && (
              <View
                style={{
                  flex: 1,
                  flexDirection: 'column',
                }}>
                <Question
                  onSelect={answer => checkAnswer(current, answer, id)}
                  title={title}
                  question={questionsFiltered[current]}
                  total={questionsFiltered.length}
                  results={results}
                  current={current}
                />
                {(results.correctAnswers.includes(current) ||
                  results.incorrectAnswers.includes(current)) &&
                  questionsFiltered[current].correctAnswerText != '' && (
                    <FadeInView duration={750} style={{alignItems: 'center'}}>
                      <TextWithFont style={styles.text}>
                        {questionsFiltered[current].correctAnswerText}
                      </TextWithFont>
                    </FadeInView>
                  )}
                <View style={{flexDirection: 'row'}}>
                  <Button
                    title={lastQuestion ? 'FINISH' : 'NEXT'}
                    buttonStyle={styles.button}
                    disabled={
                      !results.correctAnswers.includes(current) &&
                      !results.incorrectAnswers.includes(current)
                        ? true
                        : false
                    }
                    onPress={() =>
                      lastQuestion ? completeQuizEvent(id) : next(id, current)
                    }
                  />
                  <Button
                    title="RESTART QUIZ"
                    buttonStyle={styles.button}
                    onPress={() => reset(id)}
                  />
                </View>
              </View>
            )}
            {completed === true && (
              <ShareResult {...video} resetQuiz={() => reset(id)} />
            )}
          </View>
        </ScrollView>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    paddingBottom: 50,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingTop: 15,
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor: 'green',
  },
  header: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    justifyContent: 'flex-start',
    borderBottomColor: '#ffffff50',
    backgroundColor: '#00669990',
    alignItems: 'center',
    height: 50,
  },
  titleText: {fontSize: 10},

  questionCount: {
    backgroundColor: 'white',
    padding: 3,
    color: '#135291',
    width: 140,
  },
  loadingQuestions: {
    flex: 1,
  },
  title: {
    fontSize: 18,
    alignItems: 'center',
    width: Dimensions.get('window').width - 60,
  },
  image: {width: 50, height: 40, margin: 5},
  text: {
    color: '#fff',
    fontSize: 16,
    padding: 5,
    lineHeight: 20,
    marginTop: 5,
    marginLeft: 20,
    borderRadius: 3,
    backgroundColor: '#006666',
  },
  button: {
    margin: 20,
    marginBottom: 40,
    flex: 6,
    padding: 10,
    backgroundColor: '#0CA8A8',
  },
});
