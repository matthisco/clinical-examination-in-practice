import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Platform,
  TouchableWithoutFeedback,
} from 'react-native';

import StyledText from '../components/StyledText';
import {CommonActions} from '@react-navigation/native';

export default function HomeMenu(props) {
  let {style, navigation} = props;
  let fontSize = style && style.fontSize ? style.fontSize : 26;
  let backgroundColor =
    style && style.backgroundColor == 'blue' ? '' : '#00003399';
  return (
    <View style={styles.container}>
      <TouchableWithoutFeedback
        onPress={() => {
          navigation.navigate('Video Episodes');
        }}>
        <View style={{...styles.menuItem, backgroundColor}}>
          <View style={styles.menuContainer}>
            <Image
              source={require('../assets/images/video-episodes.png')}
              style={styles.icon}
            />
            <Text style={[styles.menuText]}>
              <Text style={{fontFamily: 'Lato-Bold', fontSize}}> VIDEO</Text>{" "}
              <Text style={{fontFamily: 'Lato-Light', fontSize}}>EPISODES</Text>
            </Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
      <TouchableWithoutFeedback
        onPress={() => {
          navigation.navigate('Tests');
        }}>
        <View style={{...styles.menuItem, backgroundColor}}>
          <View style={styles.menuContainer}>
            <Image
              source={require('../assets/images/test-yourself.png')}
              style={styles.icon}
            />
            <Text style={styles.menuText}>
              <Text style={{fontFamily: 'Lato-Bold', fontSize}}> TEST</Text>
              <Text style={{fontFamily: 'Lato-Light', fontSize}}>
                {' '}
                YOURSELF
              </Text>
            </Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
      <TouchableWithoutFeedback
        onPress={() => {
          navigation.navigate('My Results');
        }}>
        <View style={{...styles.menuItem, backgroundColor}}>
          <View style={styles.menuContainer}>
            <Image
              source={require('../assets/images/my-results.png')}
              style={styles.icon}
            />
            <Text style={styles.menuText}>
              <Text style={{fontFamily: 'Lato-Bold', fontSize}}> MY</Text>
              <Text style={{fontFamily: 'Lato-Light', fontSize}}> RESULTS</Text>
            </Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
      <TouchableWithoutFeedback
        onPress={() => {
          navigation.navigate('Bookmarked Videos');
        }}>
        <View style={{...styles.menuItem, backgroundColor}}>
          <View style={styles.menuContainer}>
            <Image
              source={require('../assets/images/bookmark.png')}
              style={styles.icon}
            />
            <Text style={styles.menuText}>
              <Text style={{fontFamily: 'Lato-Bold', fontSize}}>
                BOOKMARKED
              </Text>
              <Text style={{fontFamily: 'Lato-Light', fontSize}}>VIDEOS</Text>
            </Text>
          </View>
        </View>
      </TouchableWithoutFeedback>

      <TouchableWithoutFeedback
        onPress={() => {
          navigation.navigate('About');
        }}>
        <View style={{...styles.menuItem, backgroundColor}}>
          <View style={styles.menuContainer}>
            <Image
              source={require('../assets/images/about.png')}
              style={styles.icon}
            />
            <Text style={styles.menuText}>
              <Text style={{fontFamily: 'Lato-Bold', fontSize}}> ABOUT</Text>
            </Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    paddingTop: 20,
    fontFamily: 'lato-regular',
  },
  icon: {
    resizeMode: 'contain',
    width: 45,
    height: 45,
    opacity: 1,
    marginRight: 10,
  },
  menuContainer: {
    width: 220,
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
  },
  menuItem: {
    padding: 20,
    alignItems: 'center',
    width: '100%',
    opacity: 0.8,

    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  menuText: {
    color: '#fff',
    opacity: 1,
    fontFamily: 'lato-regular',
    textTransform: 'uppercase',
  },
});
