import React from 'react';
import {connect} from 'react-redux';
import {ScrollView, StyleSheet, View} from 'react-native';
import VideoList from '../components/VideoList';
import DrawerHeader from '../navigation/DrawerHeader';
class BookmarkedVideosScreen extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		const {videos} = this.props;
		const videoList = videos.filter(video => video.bookMarked == true);
		if (videos == '') {
			return (
				<View style={[styles.container, styles.horizontal]}>
					<ActivityIndicator size="large" color="#fff" />
				</View>
			);
		}

		return (
			<>
				<DrawerHeader {...this.props} />
				<ScrollView contentContainerStyle={styles.scrollContainer}>
					<VideoList videos={videoList} {...this.props.navigation} />
				</ScrollView>
			</>
		);
	}
}

const mapStateToProps = state => {
	return {
		videos: state.ceipApp.videos,
	};
};

export default connect(mapStateToProps)(BookmarkedVideosScreen);

const styles = StyleSheet.create({
	scrollContainer: {
		fontFamily: 'lato-regular',
		paddingVertical: 20,
		width: '100%',
		alignItems: 'flex-start',
		paddingTop: 15,
		flexDirection: 'row',
		flexWrap: 'wrap',
		justifyContent: 'center',
	},
	container: {
		flex: 1,
		fontFamily: 'lato-regular',
	},
});
