import React, { Component } from "react";
import PropTypes from "prop-types";

import { StyleSheet, Text } from "react-native";

const TextWithFont = props => {
	let { style, children } = props;

	return (
		<Text style={[style, { fontFamily: "Lato-Regular" }]}>{children}</Text>
	);
};

export default TextWithFont;
