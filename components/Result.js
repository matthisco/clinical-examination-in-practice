import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Platform,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StackActions, NavigationActions} from 'react-navigation';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import CustomImage from './CustomImage';
import TextWithFont from './TextWithFont';
export default class Result extends React.Component {
  constructor(props) {
    super(props);
    this.state = {height: 215};
  }

  render() {
    const {
      id,
      title,
      description,
      video,
      preview,
      push,
      dispatch,
      testLink,
      results: {totalScore},
    } = this.props;

    return (
      <TouchableOpacity
        style={styles.result}
        key={id}
        onPress={() => {
          const resetAction = StackActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                routeName: 'Drawer',
                params: {id},
              }),
            ],
          });
          const navigateAction = NavigationActions.navigate({
            routeName: 'TestYourself',
            params: {id},
          });
          this.props.navigation.dispatch(resetAction);
          this.props.navigation.dispatch(navigateAction);
        }}>
        <View style={styles.header}>
          <TextWithFont style={styles.text}>{title}</TextWithFont>
          <TextWithFont style={styles.subtext}>You Scored</TextWithFont>
          <AnimatedCircularProgress
            size={100}
            style={styles.chart}
            width={10}
            fill={totalScore}
            tintColor="#fff"
            backgroundColor="#3d5875">
            {fill => <Text style={styles.score}>{totalScore} %</Text>}
          </AnimatedCircularProgress>
        </View>
      </TouchableOpacity>
    );
  }
}
const styles = StyleSheet.create({
  result: {
    width: 170,
    margin: 3,
    opacity: 0.8,
    marginTop: 2,
    flexWrap: 'wrap',
    padding: 5,
    color: 'white',
    fontSize: 20,
  },
  text: {
    color: 'white',
    textAlign: 'center',
    paddingBottom: 3,
    borderBottomWidth: 1,
    borderColor: '#fff',
    textTransform: 'uppercase',
  },
  subtext: {
    color: 'green',
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 5,
    textTransform: 'uppercase',
  },
  chart: {
    flex: 1,
    alignSelf: 'center',
  },
  score: {
    color: 'white',
    fontFamily: 'Lato-Bold',
    fontSize: 25,
  },
  header: {
    color: 'white',
    padding: 10,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00669970',
  },
});
