import React from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableHighlight,
  Image,
  View,
  WebView,
  BackHandler,
} from 'react-native';
import {AndroidBackHandler} from 'react-navigation-backhandler';
import {useSelector, useDispatch} from 'react-redux';
import Share from 'react-native-share';

import StyledText from '../components/StyledText';
import VideoPlayerHeader from '../components/VideoPlayerHeader';

import VideoPlayer from '../components/VideoPlayer';
import {bookmarkVideo} from '../actions/videos';
import ImageButton from '../components/ImageButton';
import DrawerHeader from '../navigation/DrawerHeader';

export default function VideoPlayerScreen(props) {
  const videos = useSelector(state => state.ceipApp.videos);
  const dispatch = useDispatch();
  const {
    route: {
      params: {id},
    },
    navigation: {navigate},
  } = props;

  let videoProps = videos.find(obj => obj.id == id);

  let {
    description,
    youtubeVideo,
    title,
    recapId,
    parentId,
    questions,
  } = videoProps;
  let navId = parentId || id;
  const filteredDescription = description.replace(
    /(<p[^>]+?>|<p>|<\/p>)/gim,
    '',
  );

  const onShare = () => {
    let videoProps = videos.find(obj => obj.id == id);

    let {description, youtubeVideo, title} = videoProps;
    const url = `https://www.youtube.com/watch?v=${youtubeVideo}`;

    let shareOptions = {
      title,
      preview: 'Check out this video',
      url,
      subject: 'Share Link', //  for email
    };
    Share.open(shareOptions).catch(err => console.log(err));
  };

  const loadRecapVideo = id => {
    navigate('VideoPlayer', {id});
  };

  const navigateScreen = () => {
    navigate('Test Yourself', {id});
  };

  const bookmarkVideoEvent = () => {
    const action = bookmarkVideo(id);
    dispatch(action);
    navigate('Video Player');
  };

  return (
    <>
      <DrawerHeader {...props} />
      <View style={styles.container}>
        <ScrollView>
          <VideoPlayerHeader {...videoProps} onClick={bookmarkVideoEvent} />
          <VideoPlayer {...videoProps} />
          <View style={styles.opacityBackground}>
            <View
              style={{
                margin: 12,
              }}>
              <StyledText text="Episode Overview" />
            </View>

            <Text style={[styles.description, styles.text]}>
              {filteredDescription}
            </Text>
            {recapId ? (
              <View style={styles.section}>
                <ImageButton
                  text="Episode Recap"
                  image="recap-icon"
                  onClick={loadRecapVideo}
                />
              </View>
            ) : null}

            {questions[0].question !== '' ? (
              <View style={styles.section}>
                <ImageButton
                  text="Test Yourself"
                  image="test-yourself"
                  onClick={navigateScreen}
                />
              </View>
            ) : null}

            <View style={styles.section}>
              <ImageButton
                text="Share Video"
                image="share-icon"
                onClick={() => onShare()}
              />
            </View>
          </View>
        </ScrollView>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  opacityBackground: {
    backgroundColor: '#00333380',
    padding: 10,
    paddingBottom: 40,
    flex: 1,
  },
  text: {color: 'white'},
  description: {
    fontSize: 16,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 0,
    paddingBottom: 0,
  },
  section: {
    flexDirection: 'row',
    borderColor: '#ffffff50',
    borderBottomWidth: 1,
    paddingBottom: 15,
    paddingTop: 10,
  },
  leftContainer: {
    height: 30,
    width: 40,
    margin: 10,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  rightContainer: {
    flexDirection: 'row',
  },
});
