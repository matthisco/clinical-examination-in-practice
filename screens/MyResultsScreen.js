import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {CommonActions} from '@react-navigation/native';
import {
  ScrollView,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import TextWithFont from '../components/TextWithFont';
import StyledText from '../components/StyledText';
import DrawerHeader from '../navigation/DrawerHeader';
const MyResultsScreen = props => {
  const videos = useSelector(state => state.ceipApp.videos);
  const {
    navigation: {navigate},
  } = props;
  return (
    <>
      <DrawerHeader {...props} />
      <View
        style={{
          flex: 1,
          alignItems: 'flex-start',
          justifyContent: 'center',
        }}>
        <ScrollView contentContainerStyle={styles.scrollContainer}>
          {videos.map((item, key) => {
            const {id} = item;
            return item.completed === true ? (
              <TouchableOpacity
                style={styles.result}
                key={key}
                onPress={() => navigate('Test Yourself', {id})}>
                <View style={styles.header}>
                  <TextWithFont style={styles.text}>{item.title}</TextWithFont>
                  <TextWithFont style={styles.subtext}>You Scored</TextWithFont>
                  <AnimatedCircularProgress
                    size={100}
                    style={styles.chart}
                    width={10}
                    fill={item.results.totalScore}
                    tintColor="#fff"
                    backgroundColor="#3d5875">
                    {fill => (
                      <Text style={styles.score}>
                        {item.results.totalScore} %
                      </Text>
                    )}
                  </AnimatedCircularProgress>
                </View>
              </TouchableOpacity>
            ) : null;
          })}
        </ScrollView>
      </View>
    </>
  );
};

export default MyResultsScreen;

const styles = StyleSheet.create({
  scrollContainer: {
    paddingVertical: 20,
    width: '100%',
    alignItems: 'center',
    paddingTop: 15,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
  },
  result: {
    width: 170,
    margin: 3,
    opacity: 0.8,
    marginTop: 2,
    flexWrap: 'wrap',
    padding: 5,
    color: 'white',
    fontSize: 20,
  },
  text: {
    color: 'white',
    textAlign: 'center',
    paddingBottom: 3,
    borderBottomWidth: 1,
    borderColor: '#fff',
    textTransform: 'uppercase',
  },
  subtext: {
    color: 'green',
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 5,
    textTransform: 'uppercase',
  },
  chart: {
    flex: 1,
    alignSelf: 'center',
  },
  score: {
    color: 'white',
    fontFamily: 'Lato-Bold',
    fontSize: 25,
  },
  header: {
    color: 'white',
    padding: 10,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00669970',
  },
});
