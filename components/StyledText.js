import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {StyleSheet, Text, View} from 'react-native';

const StyledText = props => {
	let {text, bold, style} = props;
	let fontSize = style && style.fontSize ? style.fontSize : 20;
	let textTransform =
		style && style.textTransform == 'lowercase'
			? style.textTransform
			: 'uppercase';

	let firstWord = text.replace(/ .*/, '');
	let color = 'white';
	let restOfTitle =
		text.indexOf(' ') != -1 ? text.substr(text.indexOf(' ') + 1) : '';

	let firstWordFont = 'Lato-Bold';
	let restOfFont = bold ? 'Lato-Bold' : 'Lato-Light';
	return (
		<View style={styles.container}>
			<Text
				style={{
					fontFamily: firstWordFont,
					fontSize,
					textTransform,
					color,
					height: 'auto',
				}}>
				{firstWord + ' '}
			</Text>
			<Text
				style={{
					fontFamily: restOfFont,
					fontSize,
					textTransform,
					color,
					height: 'auto',
				}}>
				{restOfTitle}
			</Text>
		</View>
	);
};

export default StyledText;

const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
	},
});
