import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {
	StyleSheet,
	Text,
	View,
	Dimensions,
	Image,
	TouchableHighlight,
} from 'react-native';
import StyledText from './StyledText';
import {images} from '../helpers/images';

export default function VideoPlayerHeader(props) {
	let {title, bookMarked, icon, id, onClick} = props;

	let imageSource = images[icon].uri;
	return (
		<View style={styles.navBar}>
			<View style={styles.leftContainer}>
				<Image
					style={styles.iconStyle}
					resizeMode="contain"
					source={imageSource}
				/>
			</View>
			<View style={styles.title}>
				<StyledText text={title} style={styles.styledText} />
			</View>
			<View style={styles.rightContainer}>
				<TouchableHighlight onPress={() => onClick()}>
					{bookMarked ? (
						<Image
							style={{
								width: 25,
								height: 32,
							}}
							source={require('../assets/images/bookmark-filled.png')}
						/>
					) : (
						<Image
							style={{
								width: 25,
								height: 32,
							}}
							source={require('../assets/images/bookmark.png')}
						/>
					)}
				</TouchableHighlight>
			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	title: {
		paddingLeft: 0,
		width: Dimensions.get('window').width - 90,
	},
	styledText: {
		fontSize: 14,
		alignItems: 'center',
		backgroundColor: 'red',
	},
	navBar: {
		padding: 5,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		borderTopWidth: 1,
		borderTopColor: '#ffffff50',
		borderBottomWidth: 1,
		backgroundColor: '#00669970',
		borderBottomColor: '#ffffff50',
	},
	iconStyle: {width: 40, height: 40},

	leftContainer: {
		marginLeft: 10,
		marginRight: 10,
	},
	rightContainer: {
		height: 40,
		width: 30,
		justifyContent: 'flex-start',
		alignItems: 'center',
		paddingRight: 10,
	},
});
