import React, {Component} from 'react';

import {connect} from 'react-redux';
import {
  Image,
  View,
  Text,
  Modal,
  Button,
  TouchableOpacity,
  AsyncStorage,
  StyleSheet,
  Platform,
  Alert,
  StatusBar,
  TouchableHighlight,
} from 'react-native';
import {CommonActions} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import StyledText from '../components/StyledText';
class BasicHeader extends Component {
  constructor() {
    super();
    this.resetForm = this.resetForm.bind(this);
  }
  resetForm() {
    const {goBack, navigate} = this.props;

    const resetAction = CommonActions.reset({
      index: 0,
      actions: [
        CommonActions.navigate({
          routeName: 'Drawer',
        }),
      ],
    });

    const navigateAction = CommonActions.navigate({
      routeName: 'VideoEpisodes',
    });

    dispatch(resetAction);
    dispatch(navigateAction);
  }

  render() {
    const {navigation, videos} = this.props;
    return (
      <View style={styles.container}>
        <View>
          <TouchableHighlight
            style={{
              width: 32,
              marginLeft: 10,
              marginTop: 10,
              marginRight: 5,
              alignSelf: 'flex-start',
            }}
            onPress={this.resetForm}>
            <Icon name="chevron-left" size={20} color="#fff" />
          </TouchableHighlight>
          <View style={{paddingLeft: 5}}>
            <StyledText text="Home" />
          </View>
        </View>
        <View>
          <Image
            source={require('../assets/images/ia-logo.png')}
            style={{
              width: 140,
              height: 40,
              marginRight: 5,
              padding: 10,
            }}
          />

          <TouchableOpacity
            onPress={() => {
              this.props.navigation.openDrawer();
            }}
            style={{
              margin: 5,
              alignSelf: 'flex-end',
            }}>
            <Icon name="bars" size={20} color="#fff" />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default BasicHeader;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'green',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: Platform.OS === 'ios' ? 50 : 10,
    minHeight: 50,
    position: 'relative',
  },
  title: {
    color: '#fff',
    fontSize: 18,
  },
  text: {
    color: '#fff',
    fontSize: 14,
  },
});
